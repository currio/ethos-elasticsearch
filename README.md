# ethOS ElasticSearch

Allow ethOS rigs to dump data to ElasticSearch

## Getting Started

These instructions will allow your rig to send monitoring data to an ElasticSearch instance.

### Prerequisites

* Make sure you have EthOS 1.2.7 at least.
* Install ElasticSearch and Kibana.
* Create an index and make sure it has a timestamp field. From Kibana's **Console** in **Dev Tools**:
    * Create the index with `put ethos`
	  * Add the timestamp field:

            put ethos/_mapping/doc
            {
              "properties": {
                  "timestamp": {
                    "type": "date"
                  }
                }
            }
* Create an index pattern `ethos*`

### Installing

First, clone this repository

```
git clone https://bitbucket.org/currio/ethos-elasticsearch.git
```

Add an `es_server` field in your **local.conf** pointing to the ElasticSearch index, e.g.:

```
# ...
es_server http://102.192.168.0.10:9200/ethos/doc

# ...
```

Test you're sending data properly. From the repo's directory, run:

```
$ ./bin/update-es
```

Once the script is done, refresh kibana (including a refresh of the index pattern) and check out the *Discover* panel for new data.

You can then build e.g. a *TimeLion* series with an expression like:

```
.es(metric=min:status.a_temp).label("min"),
.es(metric=max:status.a_temp).label("max"),
.es(metric=avg:status.a_temp).label("avg")
```
which will display the evolution of temperatures (min, max and average).

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Laur Ivan** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* ElasticSearch and Kibana teams