<?php
require_once('/opt/ethos/lib/functions.php');

// Utility functions to convert strings to numeric values
//
function to_num_array($str) {
	return array_map('floatval', explode(' ',$str));	
}

function avg($a) {
	return array_sum($a)/count($a);
}

// Convert strings to numeric values
//
function process_stats() {
  $send = get_stats();

  $send['cores'] = to_num_array($send['core']);
	$send['voltages'] = to_num_array($send['voltage']);
	$send['miner_hash_values'] = to_num_array($send['miner_hashes']);
	$send['fan_rpms'] = to_num_array($send['fanrpm']);
	$send['fan_percents'] = to_num_array($send['fanpercent']);
	$send['powertunes'] = to_num_array($send['powertune']);
	$send['vramsizes'] = to_num_array($send['vramsize']);
	$send['a_bioses'] = explode(' ', $send['bioses']);
	$send['cpu_temp'] = floatval($send['cpu_temp']);

	$send['avg_core'] = avg($send['cores']);
	$send['avg_rpms'] = avg($send['fan_rpms']);
	$send['avg_hashes'] = avg($send['miner_hash_values']);
	$send['gpus'] = floatval($send['gpus']);
  $send['sum_hashes'] = array_sum($send['miner_hash_values']);
  $send['val_hash'] = floatval($send['hash']);

  $send['a_temp'] = to_num_array($send['temp']);

  $send['val_tx_kbps'] = to_num_array($send['tx_kbps']);
  $send['val_rx_kbps'] = to_num_array($send['rx_kbps']);

  $send['a_default_core'] = to_num_array($send['default_core']);
  $send['a_default_mem'] = to_num_array($send['default_mem']);
  $send['a_core'] = to_num_array($send['core']);
  $send['a_mem'] = to_num_array($send['mem']);

  return $send;
  
}

function send_post($url, $hash, $value)
{
	$data = array("rig_name" => $hash, "status" => $value, "timestamp" => time()*1000);
	$data_string = json_encode($data);
	$ch = curl_init($url);
	if ($ch) {
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string)
			)
		);
    $result = curl_exec($ch);
    print_r($result);
		return $result;
	}
	return "OUCH!, cannot send stuff via CURL!";
}

function send_data_es($hook)
{
	$send = process_stats();
	$private_hash = trim(file_get_contents("/var/run/ethos/private_hash.file"));
  $public_hash = trim(file_get_contents("/var/run/ethos/panel.file"));
  
	$log = "";
	foreach($send as $key => $data) {
		$log.= "$key:$data\n";
  }
  
  $message = send_post($hook, $private_hash, $send);
	file_put_contents("/opt/ethos/etc/message", trim($message));
	return $log;
}
?>